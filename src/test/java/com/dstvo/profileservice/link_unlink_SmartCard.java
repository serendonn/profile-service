package com.dstvo.profileservice;

import com.dstvo.helpers.APIRequestUtils;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;



public class link_unlink_SmartCard extends APIRequestUtils{


    @Test(priority = 1)
    public void link_Smartcard(){

        JSONObject requestParams = new JSONObject();
        requestParams.put("smartcardNumber", smartcardNumber);
        requestParams.put("country", country);

        System.out.println("Running test...Linking Smartcard");
        given().
                contentType("application/json").
                body(requestParams.toJSONString()).
        when().
                post(connectId+"/smartcards").
        then().
                log().everything().
                statusCode(200);

        }


    @Test(priority = 2)
    public void getListof_Smartcards() throws InterruptedException{
        Thread.sleep(7000);
        System.out.println("Get List of Smartcards...");

        given().
                contentType("application/json").
        when().
                get(connectId+"/smartcards").
        then().
                log().body().
                statusCode(200).body("items.smartCardNumber", contains(smartcardNumber));
    }


    @Test(priority = 3)
    public void unlink_Smartcard() throws InterruptedException {
        System.out.println("Running test...Unlinking Smartcard");
        given().
                contentType("application/json").
        when().
                delete(connectId+"/smartcards/"+smartcardNumber).
        then().
                log().everything().
                statusCode(200);
    }


    @Test(priority = 4)
    public void getListOf_Smartcards_1() throws InterruptedException{
        Thread.sleep(7000);
        System.out.println("Get List of Smartcards...Is smartcard removed?");

        given().
                contentType("application/json").
                when().
                get(connectId+"/smartcards").
                then().
                log().body().
                statusCode(200).body("items.smartCardNumber", not(contains(smartcardNumber)));
    }


}
